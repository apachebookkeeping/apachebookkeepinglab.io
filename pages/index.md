---
permalink: /
layout: default-single-column
title: Apache Bookkeeping | Small Business Financial Support Specialist
stylesheets:
  - link: /styles/home.css
    type: text/css
description: Apache Bookkeeping supporting natural-health small businesses with a holistic view business management
social-media:
  image: /images/social-media-cards/two-women-1920x1080.jpg
---
{% assign ns = "banner" %}
<article class="{{ ns }}">
  <picture class="{{ ns }}__background">
    <source srcset="/images/pages/home/splash-background-2000w.jpg" media="(min-width: 1350px)">
    <source srcset="/images/pages/home/splash-background-1350w.jpg" media="(min-width: 960px)">
    <source srcset="/images/pages/home/splash-background-960w.jpg" media="(min-width: 640px)">
    <source srcset="/images/pages/home/splash-background-640w.jpg" media="(min-width: 480px)">
    <img class="{{ ns }}__background__image" src="/images/pages/home/splash-background-480w.jpg" alt="splash">
  </picture>
  
  <section class="{{ ns }}__overlay">
    <section class="{{ ns }}__info">
      <section class="{{ ns }}__info__content" data-theme="white">
        <h1 class="{{ ns }}__head">Easing Financial Uncertainty For <span style="color: #b89600">Extraordinary</span> Professionals</h1>
        
        <a class="{{ ns }}__button" data-theme="dark" href="{{ site.data.pages.about.link }}">
          Learn More
        </a>
      </section>
    </section>
  </section>
</article>

{% assign ns = "summary-1" %}
<article class="{{ ns }}">
  <img
    class="{{ ns }}__image"
    src="/images/pages/home/summary-1-360w.jpg"
    srcset="/images/pages/home/summary-1-360w.jpg 360w,
            /images/pages/home/summary-1-640w.jpg 640w"
    sizes="(max-width: 360px) 360px,
           640px"
  >
  
  {% assign head-class = ns | append: "__head" %}
  {% include element/header.md 
    head = "Taking your practice to the next level can be a struggle"
    type = "section"
    theme = "dark"
    class = head-class
  %}
  
  <section class="{{ ns }}__list">
    <li>What is there a better way to track my mamas and inventory?</li>
    <li>What can I do to spend more time with my mamas?</li>
    <li>What is the best method to expedite payments?</li>
    <li>What is holding my business back from growing?</li>
    <li>How can I do this if I am not a numbers person?</li>
  </section>
  
  {% assign button-class = ns | append: "__button" %}
  {% include element/button.md 
    link = site.data.pages.contact.link
    text = "Let's get Your Questions Answered"
    theme = "dark"
    class = button-class
  %}
</article>

{% assign ns = "summary-2" %}
<article class="{{ ns }}">
  <img
    class="{{ ns }}__image"
    src="/images/pages/home/summary-2-360w.jpg"
    srcset="/images/pages/home/summary-2-360w.jpg 360w,
            /images/pages/home/summary-2-640w.jpg 640w"
    sizes="(max-width: 360px) 360px,
           640px"
  >
  
  {% assign head-head = "That’s why we specialize in providing bookkeeping and advisory services to reproductive **healthcare professionals**." | markdownify %}
  {% assign head-class = ns | append: "__head" %}
  {% include element/header.md 
    head = head-head
    type = "section"
    theme = "dark"
    class = head-class
  %}
  
  <section class="{{ ns }}__text">
    {{ "Our mission is to improve your accounting processes and maximize your bottom line. We use our proven **three part methodology** to take your business to the next level." | markdownify }}
  </section>
  
  {% assign button-class = ns | append: "__button" %}
  {% include element/button.md 
    link = site.data.pages.services.link
    text = "Learn More"
    theme = "dark"
    class = button-class
  %}
</article>

{% include section/methodology.html %}
{% include section/testimonials.html %}

