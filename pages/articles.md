---
permalink: /articles/
layout: default-body-sidebar
title: Articles | Apache Bookkeeping
stylesheets:
  - link: /styles/articles.css
    type: text/css
description: Read articles from our wonderful authors about health, wellness, and finances that will enrich your life and business. 
social-media:
  image: /images/social-media-cards/baby-1920x1080.jpg
---

{% assign ns = "articles" %}
<section class="{{ ns }}">
  {% assign ns = "main" %}
  <article class="{{ ns }}">
    {% assign ns = "main__head" %}
    <header class="{{ ns }}">
      <h1 class="{{ ns }}__title">Enjoy Our Articles</h1>
      <p class="{{ ns }}__summary">
        Read articles from our wonderful authors about health, wellness, and finances that will enrich your life and business.
      </p>
      <img
        class="{{ ns }}__image"
        src="/images/pages/articles-360w.jpg"
        srcset="/images/pages/articles/main-360w.jpg 360w,
                /images/pages/articles/main-640w.jpg 640w"
        sizes="(max-width: 360px) 360px,
               640px"
      >
      <a class="{{ ns }}__button" href="" data-theme="dark">Sign-up for our Newsletter</a>
    </header>
    
    {% assign ns = "main__articles" %}
    <section class="{{ ns }}">
      {% for article in site.posts limit: 4 %}
        {% assign writer = site.writers | where: 'guid', article.writer | first %}
        {% assign ns = "main__article" %}
        <article class="{{ ns }}" data-theme="main">
          <section class="{{ ns }}__head">
            <a href="{{ writer.url }}">
              <img class="{{ ns }}__image" src="{{ writer.icon }}">
            </a>
          </section>

          <section href="{{ article.url }}" class="{{ ns }}__body" data-theme="main">
            {% assign ns = "main__article__body" %}
            <h3 class="{{ ns }}__title">{{ article.title }}</h3>
            <section class="{{ ns }}__text">{{ article.excerpt | markdownify }}</section>
            {% include element/button.md
              theme="dark"
              link=article.url
              text="Read More"
              class="main__article__body__button"
            %}
            <a href="{{ writer.url }}" class="{{ ns }}__writer">{{ writer.firstname }}</a>
          </section>
        </article>
      {% endfor %}
    </section>
  </article>

  {% assign ns = "more" %}
  <section class="{{ ns }}" data-theme="white">

    {% assign ns = "more__recent" %}
    <section class="{{ ns }}">
      <h3 class="{{ ns }}__title">Recent Articles</h3>
      <section class="{{ ns }}__list">
      {% for article in site.posts limit: 10 %}
        <a class="{{ ns }}__item" href="{{ article.url }}">{{ article.title }}</a>
      {% endfor %}
      </section>
    </section>
    
    {% assign ns = "more__history" %}
    <section class="{{ ns }}">
      <h3 class="{{ ns }}__title">All Articles</h3>
      {% assign articlesByYear = site.posts | group_by_exp: "post", "post.date | date: '%Y'" %}
      
      {% for year in articlesByYear %}
        <details>
          <summary class="{{ ns }}__year">{{ year.name }}</summary>
          <section class="{{ ns }}__year__list">
            {% for article in year.items %}
              <a class="{{ ns }}__year__item" href="{{ article.url }}">{{ article.title }}</a>
            {% endfor %}
          </section>
        </details>
        
      {% endfor %}
    </section>
  </section>
  
</section>


