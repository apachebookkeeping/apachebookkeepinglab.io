---
permalink: /resources/
layout: default-single-column
title: Resources | Apache Bookkeeping
stylesheets:
  - link: /styles/resources.css
    type: text/css
description: Resources that Apache Bookkeeping has hand selected to assist you on your journey to secure independence
social-media:
  image: /images/social-media-cards/smart-phone-1920x1080.jpg
---

{% assign ns = "head" %}
<header class="head">
  <h1 class="{{ ns }}__title" data-theme="dark">Resources</h1>
  <p class="{{ ns }}__text">
    Over the years we have found site, program, companies that have greatly help us or others. Take a look around and find extra help.
  </p>
  <img
    class="{{ ns }}__image"
    src="/images/pages/resources/main-360w.jpg"
    srcset="/images/pages/resources/main-360w.jpg 360w,
            /images/pages/resources/main-640w.jpg 640w"
    sizes="(max-width: 360px) 360px,
           640px"
  >
</header>

{% assign ns = "resources" %}
<section class="{{ ns }}">
  {% assign ns = ns | append: "__resource" %}
  {% for resource in site.data.resources %}
    <article class="{{ ns }}" data-theme="dark">
      <img class="{{ ns }}__head" src="{{ resource.image }}">
      <section class="{{ ns }}__box">
        <h3 class="{{ ns }}__title">{{ resource.title }}</h3>
        <section class="{{ ns }}__summary">{{ resource.text }}</section>
        <a class="{{ ns }}__button" data-theme="light" href="{{ resource.button-link }}" {% if resource.button-type and resource.button-type == "external" %}target="_blank"{% endif %}>{{ resource.button-text }}</a>
      </section>
    </article>
  {% endfor %}
</section>

