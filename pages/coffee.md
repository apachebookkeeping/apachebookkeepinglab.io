---
{
  "permalink": "/coffee/",
  "title": "Contact Us | Apache Bookkeeping",
  "layout": "default-single-column",
  "metas": [
    [
      { "key": "http-equiv", "value": "refresh" },
      { "key": "content", "value": "0;url=/contact#schedule" },
      { "key": "robots", "value": "noindex" }
    ]
  ],
  "redirect": "/contact#schedule",
  "description": "Ready to book a consultation to great freedom from the back office and back into your client and family's life again",
  "canonical": "https://apachebookkeeping.com/contact#schedule",
  "social-media": {
    "image": "/images/social-media-cards/smart-phone-1920x1080.jpg"
  }
}    
---

<style>
  .link {
    color: var(--theme-text__color);
    text-decoration: underline;
    transition: color 150ms linear;
  }
  
  .link:hover {
    color: var(--theme-text__color--hover);
  }
</style>

<div class="container" style="text-align: center">
  <h1>301</h1>

  <p><strong>Moved Permanently</strong></p>
  <p><a class="link" data-theme="link" href="{{ page.redirect }}">Click here</a> if page does not automatically redirect you.</p>
</div>

