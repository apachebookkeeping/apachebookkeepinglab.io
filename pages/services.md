---
permalink: /services/
layout: default-single-column
title: Services | Apache Bookkeeping
stylesheets:
  - link: /styles/services.css
    type: text/css
description: Apache Bookkeeping supports a wide range of business needs from basic monthly record keeping to full management support
social-media:
  image: /images/social-media-cards/smart-phone-1920x1080.jpg
---
{% assign ns = "extra-before" %}
<article class="{{ ns }}" data-theme="dark">
  <section class="{{ ns }}__content">
    <h2 class="{{ ns }}__head">
      What We Offer
    </h2>
    
    <section class="{{ ns }}__text">
      <img 
        class="{{ ns }}__image"
        src="/images/pages/services/main-360w.jpg"
        srcset="/images/pages/services/main-360w.jpg 360w,
                /images/pages/services/main-640w.jpg 640w"
        sizes="(max-width: 360px) 360px,
               640px">
      <p>Is your heart ready to change the world but no matter what you do you have tried you are hitting the wall of dealing with day to day task?  You would like to take on more clients but you run out of time and cannot offer the level of care that you know that they deserve? You know what you should do, but the very thought of dealing with your receipt, invoices, and taxes fill your mouth with bile. Maybe it is time to take those dreaded task off your plate permanently and rediscover your joy.</p>
      <p>Apache Bookkeeping got you and by taking our three step methodology to take your practice to where you want to go.</p>
    </section>

    
    <a class="{{ ns }}__button" data-theme="main" href="/contact#schedule">
      Schedule Your Consultation Now
    </a>
  </section>
</article>

<h1 class="head" data-theme="dark">How We Service You</h1>

{% include section/methodology.html %}

{% assign ns = "additional-services" %}
{% assign ns-card = ns | append: "__card" %}
<article class="{{ ns }}">
  <h2 class="{{ ns }}__head">
    Additional Services
  </h2>
  
  <section class="{{ ns }}__list">
    {% for service in site.data.services %}
      <section class="{{ ns-card }}">
        <h3 class="{{ ns-card }}__head">{{ service.title }}</h3>
        <section class="{{ ns-card }}__text">{{ service.text | markdownify }}</section>
      </section>
    {% endfor %}
  </section>
</article>

{% assign ns = "extra-after" %}
<article class="{{ ns }}" data-theme="dark">
  <section class="{{ ns }}__content">
    <h2 class="{{ ns }}__head">We Are Here To Help</h2>
    
    <section class="{{ ns }}__text">
      <p>If the possibility getting your record-keeping done and gain back your valuable time back appeals to you, schedule your 20 minute chat today. Your finances will thank you for it.</p>
    </section>
    
    <a class="{{ ns }}__button" data-theme="main" href="/contact#schedule">Schedule Your Consultation Now</a>
  </section>
</article>

{% include section/testimonials.html %}
