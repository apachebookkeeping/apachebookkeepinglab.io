---
permalink: /resources/software-demo/
layout: default-single-column
title: Test Drives | Apache Bookkeeping
stylesheets:
  - link: /styles/resources/software-demo.css
    type: text/css
description: Want to test out accounting softward before committing to one. We have serveral options from Quickbooks to Xero that allow you to play with the software before you buy.
social-media:
  image: /images/social-media-cards/smart-phone-1920x1080.jpg
---

{% assign ns = "head" %}
<header class="head">
  <h1 class="{{ ns }}__title" data-theme="dark">Test Drives</h1>
  <p class="{{ ns }}__text">
    Want to test out accounting softward before committing to one. We have serveral options from Quickbooks to Xero that allow you to play with the software before you buy.
  </p>
</header>

{% assign ns = "resources" %}
<section class="{{ ns }}">
  {% assign ns = ns | append: "__resource" %}
  {% for resource in site.data.resources-software-demo %}
    <article class="{{ ns }}" data-theme="dark">
      <img class="{{ ns }}__head" src="{{ resource.image }}">
      <section class="{{ ns }}__box">
        <h3 class="{{ ns }}__title">{{ resource.title }}</h3>
        <section class="{{ ns }}__summary">{{ resource.text }}</section>
        <a class="{{ ns }}__button" data-theme="light" href="{{ resource.button-link }}" {% if resource.button-type and resource.button-type == "external" %}target="_blank"{% endif %}>{{ resource.button-text }}</a>
      </section>
    </article>
  {% endfor %}
</section>

{% assign ns = "back-link" %}
<section class="{{ ns }}">
  <a data-theme="dark" class="{{ ns }}__button" href="/resources">Back to Resources</a>
</section>

