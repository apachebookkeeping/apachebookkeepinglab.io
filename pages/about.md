---
permalink: /about/
layout: default-single-column
title: Who We Are | Apache Bookkeeping
stylesheets:
  - link: /styles/about.css
    type: text/css
description: Meet the team who is here to help you grow your practice and enable your attention back on your client and family's need
social-media:
  image: /images/social-media-cards/two-women-1920x1080.jpg
---

{% assign ns = "history" %}
<article class="{{ ns }}">
  <h1 class="{{ ns }}__title" data-theme="dark">About Us</h1>
  
  <section class="{{ ns }}__text">
    <p class="{{ ns }}__paragraph">
      We believe that everyone deserves the choice for their health. There should never be a one-side fit all approach. Each person is unique. Because of that, we believe in supporting those that give others the option to take charge of their lives. Let's blaze the trail for a healthier tomorrow together. 
    </p>
  </section>
  
  <footer class="{{ ns }}__buttons">
    <a class="{{ ns }}__button" data-theme="dark" href="/services">
      Services
    </a>
    <a class="{{ ns }}__button" data-theme="dark" href="/contact#schedule">
      Consultation
    </a>
  </footer>
</article>

{% assign ns = "employees" %}
{% assign ns-employee = ns | append: "__employee" %}
{% assign employee-image-class = ns-employee | append: "__image" %}
<article class="{{ ns }}" data-theme="white">
  <section class="{{ ns }}__content">
    <h2 class="{{ ns }}__head">Our Staff</h2>
    
    <section class="{{ ns }}__employees">
      {% for employee in site.data.employees %}
        <article class="{{ ns-employee }}">
          {% if employee.image-type and employee.image-sources %}
            {% include element/image.md 
              type = employee.image-type 
              sources = employee.image-sources
              class = employee-image-class
            %}
          {% endif %}
          
          <h3 class="{{ ns-employee }}__title">
            {% if employee.name %}<section class="{{ ns-employee }}__name">{{ employee.name }}</section>{% endif %}
            {% if employee.position %}<section class="{{ ns-employee }}__position">{{ employee.position }}</section>{% endif %}
          </h3>
          
          {% if employee.skills %}
            <section class="{{ ns-employee }}__skills">
              {% for skill in employee.skills %}
                <section class="{{ ns-employee }}__skill">{{ skill }}</section>
              {% endfor %}
            </section>
          {% endif %}
          
          {% if employee.quote %}<section class="{{ ns-employee }}__quote">{{ employee.quote | markdownify }}</section>{% endif %}
        </article>
      {% endfor %}
    </section>
  </section>
</article>

{% include section/testimonials.html %}
