---
permalink: /testimonials/
layout: default-single-column
title: Testimonials | Apache Bookkeeping
stylesheets:
  - link: /styles/testimonials.css
    type: text/css
description: Love Notes of appreciation given to Apache Bookkeeping for their exploratory financial and advisory services
social-media:
  image: /images/social-media-cards/two-women-1920x1080.jpg
---

{% assign ns = "head" %}
<h1 class="{{ ns }}" data-theme="dark">
  Testimonials
</h1>

{% assign ns = "testimonials" %}
<section class="{{ ns }}">
  {% assign ns = ns | append: "__testimonial" %}
  {% for testimony in site.data.testimonials %}
    <article class="{{ ns }}">
      <img class="{{ ns }}__head" src="{{ testimony.image }}">
      <section class="{{ ns }}__box">
        <section class="{{ ns }}__quote">{{ testimony.quote | markdownify }}</section>
        <footer class="{{ ns }}__reference">{{ testimony.name }}</footer>
      </section>
    </article>
  {% endfor %}
</section>

{% assign ns = "extra" %}
<section class="{{ ns }}" data-theme="white">
  <h2 class="{{ ns }}__head">Grow Your Business With Us</h2>
  <section class="{{ ns }}__summary">Today is the day to build the business of your dreams. Let's talk about how optimizing your accounting processes can take your business to the next level.</section>
  {% assign ns-button = ns | append: "__button" %}
  {% include element/button.md 
    link = "/contact#schedule"
    theme = "dark"
    text = "Schedule Your Free Consultation Now"
    class = ns-button
  %}
</section>
