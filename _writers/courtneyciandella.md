---
guid: "courtneyciandella"
title: "Courtney Ciandella | Apache Bookkeeping"
fullname: "Courtney Ciandella"
firstname: "Courtney"
lastname: "Ciandella"
business: "Freelance Content Writer"
image: "/images/writers/courtneyciandella-677w.png"
icon: "/images/writers/courtneyciandella-128w.png"
links:
  - text: "Visit Courtney Ciandella"
    link: "https://www.courtneyciandella.com/"
  - text: "courtneybciandella@gmail.com"
    link: "mailto://courtneybciandella@gmail.com"
---
Courtney Ciandella is a freelance content writer and yoga instructor focused on helping wellness professionals educate others on living a healthy lifestyle. When she is not writing, she enjoys reading, going to the beach with friends, and taking long walks with her 2-year-old dachshund-mix, Winnie.
