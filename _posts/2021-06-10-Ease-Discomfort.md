---
writer: "courtneyciandella"
title: "Remedies to Ease Discomfort During Pregnancy"
excerpt: "The entire process sounds so joyful (and it is!), but any woman who has experienced pregnancy before can attest that there are many uncomfortable side effects to pregnancy as well. While it is recommended to check in with your healthcare provider if you have any concerns throughout your pregnancy, there are some remedies you can try on your own to ease any discomfort."
catagories:
  - "health"
  - "pregnancy"
---
Pregnancy is a beautiful journey and bonding experience for mothers with their unborn babies. It’s astounding how a woman’s belly becomes this safe, nurturing environment for their babies as they grow and develop in a few months. It’s hard to believe that women’s bodies are capable of hosting such a natural process, responsible for growing a life until it’s ready to be birthed into the world. 

The entire process sounds so joyful (and it is!), but any woman who has experienced pregnancy before can attest that there are many uncomfortable side effects to pregnancy as well. While it is recommended to check in with your healthcare provider if you have any concerns throughout your pregnancy, there are some remedies you can try on your own to ease any discomfort. 

**Heartburn**
<br>
An annoying burning sensation in your chest is an unfortunate side effect that many soon-to-be mothers deal with often. It is prevalent, essentially caused by acid reflux. There are a few ways to combat heartburn, such as:
* Avoiding certain foods that seem to be the cause.
* Engaging in moderate to physical activity.
* Consuming smaller portions and eating slower to allow the food time to digest.
* Going on walks after meals could help. 

**Gas**
<br>
Having a bad case of gas seems unavoidable when you are pregnant, but there are some ways to keep it under control. Try lying down and practicing abdominal breathing (belly rises on the inhale and relaxes on the exhale). You could also rock along your pelvis to move some of the tension around, and walking after your meals to get the body moving can work as well. 

**Fatigue**
<br>
Being pregnant wipes you out. Just think, you have an entire human being growing inside you, consuming a lot of your energy. In most cases, this is normal - again, check with your doctor for this possibly could be signs of anemia or dehydration. Otherwise, you may just be overdoing it. Cut down your hours if you are still working throughout your pregnancy, get more rest and incorporate naps into your days. It’s also essential to maintain good nutrition and drink more water, which may be the fuel you need. 

**Sleeping problems**
<br>
Although you’d like to believe that your baby understands when it’s time for bed, they are not always on the same sleep schedule as you, making getting quality rest more difficult. Regular exercise is encouraged to improve sleep and daily meditation practice, which can calm the mind and body. Also, taking routine naps and sleeping with a pillow supporting the back and knees can alleviate some of the discomforts at night. Another helpful tip: drink something warm before bed like milk or chamomile tea and you’ll be sleeping in no time. 

Many other common side effects arise during pregnancy, so constant communication with your doctor or midwife is essential. Consider some of these techniques to ease discomfort. And most importantly, remain in tune with what your body is trying to tell you.
