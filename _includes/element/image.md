{% if include.type == "changing" %}
  <picture>
    {% assign sources = include.sources | sort: "width" | reverse %}
    {% for source in sources %}
      {% if forloop.last %}
        <img 
          {% if include.class %}class="{{ include.class }}"{% endif %}
          src="{{ source.src }}">
      {% else %}
        <source srcset="{{ source.src }}" media="(min-width: {{ sources[forloop.index].width }}px)">
      {% endif %}
    {% endfor %}
  </picture>
{% elsif include.type == "scaling" %}
  <img 
    {% if include.class %}class="{{ include.class }}"{% endif %}
    {% for source in include.sources | sort: "width" %}
      {% if forloop.length == 1 %}
        src="{{ source.src }}"
        srcset="{{ source.src }} {{ source.width }}w"
      {% elsif forloop.first %}
        src="{{ source.src }}"
        srcset="{{ source.src }} {{ source.width }}w
      {% elsif forloop.last %}
        , {{ source.src }} {{ source.width }}w"
      {% else %}
        , {{ source.src }} {{ source.width }}w
      {% endif %}
    {% endfor %}
  >
{% else %}
  <img 
    {% if include.class %}class="{{ include.class }}"{% endif %}
    src="{{ include.sources[0].src }}"
  >
{% endif %}
