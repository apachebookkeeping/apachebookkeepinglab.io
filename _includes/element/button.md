{% if include.link %}
  <a 
    class="button{% if include.class %} {{ include.class }}{% endif %}" 
    {% if include.theme %}data-theme="{{ include.theme }}"{% endif %}
    {% if include.link %}href="{{ include.link }}"{% endif %}
  >
    {{ include.text | default: "Learn More" }}
  </a>
{% endif %}
