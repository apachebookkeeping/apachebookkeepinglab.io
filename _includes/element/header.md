{% if include.type == "page" %}
  {% assign head-tag = "h1" %}
  {% assign summary-tag = "h3" %}
{% elsif include.type == "section" %}
  {% assign head-tag = "h2" %}
  {% assign summary-tag = "h3" %}
{% comment %}include.type == "subsection"{% endcomment %}
{% else %}
  {% assign head-tag = "h3" %}
  {% assign summary-tag = "h4" %}
{% endif %}

{% if include.head and include.summary %}
  <hgroup
    {% if include.class %}class="{{ include.class }}"{% endif %}
    {% if include.theme %}data-theme="{{ include.theme }}"{% endif %}
  >
    <{{ head-tag }}>{{ include.head }}</{{ head-tag }}>
    <{{ summary-tag }}>{{ include.summary }}</{{ summary-tag }}>
  </hgroup>
{% elsif include.head %}
  <{{ head-tag }}
    {% if include.class %}class="{{ include.class }}"{% endif %}
    {% if include.theme %}data-theme="{{ include.theme }}"{% endif %}
  >
    {{ include.head }}
  </{{ head-tag }}>
{% elsif include.summary %}
  <section
    {% if include.class %}class="{{ include.class }}"{% endif %}
    {% if include.theme %}data-theme="{{ include.theme }}"{% endif %}
  >
    {{ include.summary }}
  </section>
{% endif %}

